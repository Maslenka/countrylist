<?php

namespace App\Livewire;

use App\Models\CurrencyRate;
use Livewire\Component;
use App\Models\Country;

class Countries extends Component
{
    public $countries;
    public $data;
    public $country_selected = false;
    public $country;

    public $chart_data;

    public function showCountry($id)
    {
        $this->country = Country::where('id', $id)->with(['currencies'])->limit(30)->first();
        $this->country_selected = true;

        $this->chart_data = array();
        foreach($this->country->currencies as $currency) {
            //$this->rates[$currency->id] = CurrencyRate::where('currency_id', $currency->id)->orderBy('date', 'DESC')->limit(30)->get()->reverse();

            $labels = array(); $data = array();
            $rates = CurrencyRate::where('currency_id', $currency->id)->orderBy('date', 'DESC')->limit(30)->get()->reverse();
            if (count($rates)) {
                foreach ($rates as $rate) {
                    $labels[] = $rate->date;
                    $data[] = $rate->rate;
                }
                $this->chart_data[$currency->id] = [
                    'labels' => $labels,
                    'data' => $data,
                ];
            }
        }

        $this->dispatch('contentChanged', charts: $this->chart_data );

    }

    public function render()
    {
        $this->countries = Country::orderBy('common_name')->get();

        $this->data = [
            'labels' => ['January', 'February', 'March', 'April', 'May'],
            'data' => [65, 59, 80, 81, 56],
        ];

        return view('livewire.countries');
    }
}
