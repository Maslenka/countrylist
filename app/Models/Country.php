<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';

    protected $fillable = [
        'id',
        'iso',
        'name',
        'common_name',
        'population',
        'timezone',
        'flag_path',
    ];

    public function currencies()
    {
        return $this->belongsToMany('App\Models\Currency', 'country_currencies');
    }
}
