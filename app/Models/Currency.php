<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table = 'currencies';

    protected $fillable = [
        'id',
        'iso',
        'name',
        'sign',
    ];

    public function countries()
    {
        return $this->belongsToMany('App\Models\Country', 'country_currencies');
    }

    public function rates()
    {
        return $this->hasMany('App\Models\CurrencyRate');
    }
}
