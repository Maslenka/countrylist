<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CurrencyRate extends Model
{
    protected $fillable = [
        'id',
        'currency_id',
        'date',
        'rate'
    ];

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }
}
