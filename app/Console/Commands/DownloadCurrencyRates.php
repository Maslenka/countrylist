<?php

namespace App\Console\Commands;

use App\Models\Currency;
use App\Models\CurrencyRate;
use Illuminate\Console\Command;
use Exception;

class DownloadCurrencyRates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'download:currency-rates {--last-90-days : Download data for last 90 days }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download or update currency rates';

    public function __construct($argInput = "")
    {
        parent::__construct();

        /* Initialize output style needed for progress bar */
        $this->input = new \Symfony\Component\Console\Input\StringInput($argInput);
        $this->outputSymfony = new \Symfony\Component\Console\Output\ConsoleOutput();
        $this->outputStyle = new \Illuminate\Console\OutputStyle($this->input, $this->outputSymfony);
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        if ($this->options()['last-90-days'])
            $filepath = env("RESOURCE_URL_CURRENCY_RATES_90") or throw new Exception('Environment variable RESOURCE_URL_CURRENCY_RATES_90 not specified.');
        else
            $filepath = env("RESOURCE_URL_CURRENCY_RATES") or throw new Exception('Environment variable RESOURCE_URL_CURRENCY_RATES not specified.');

        $data = file_get_contents($filepath) or throw new Exception('Unable to fetch file from ' . $filepath);

        $currency_rates = simplexml_load_string($data);

        /* 1st level Cube - just a wrapper; 2nd level Cube - day; 3rd level Cube - currency; ...  */

        if (!$this->options()['quiet']) $this->info("Downloading currencies...\n");

        foreach ($currency_rates->Cube->Cube as $key1 => $date_raw) {
            $date = $date_raw->attributes()['time'];

            if (!$this->options()['quiet']) $this->info($date);
            if (!$this->options()['quiet']) $this->outputStyle->progressStart(count($date_raw->Cube));

            foreach ($date_raw->Cube as $key2 => $rate_raw) {
                $currency_code = $rate_raw->attributes()['currency'];
                $currency_rate = $rate_raw->attributes()['rate'];

                $currency = Currency::where('iso', $currency_code)->first();
                if ($currency) {
                    CurrencyRate::firstOrCreate(
                        [
                            'currency_id' => $currency->id,
                            'date' => $date,
                        ],
                        [
                            'rate' => $currency_rate,
                        ]
                    );
                }
                if (!$this->options()['quiet'])  $this->outputStyle->progressAdvance();
            }
            if (!$this->options()['quiet']) $this->outputStyle->progressFinish();
        };
    }
}
