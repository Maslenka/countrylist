<?php

namespace App\Console\Commands;

use App\Models\Country;
use App\Models\Currency;
use Illuminate\Console\Command;
use Exception;

class DownloadCountries extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'download:countries';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download or update list of countries from restcountries.com';

    public function __construct($argInput = "")
    {
        parent::__construct();

        /* Initialize output style needed for progress bar */
        $this->input = new \Symfony\Component\Console\Input\StringInput($argInput);
        $this->outputSymfony = new \Symfony\Component\Console\Output\ConsoleOutput();
        $this->outputStyle = new \Illuminate\Console\OutputStyle($this->input, $this->outputSymfony);
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $filepath = env("RESOURCE_URL_COUNTRIES") or throw new Exception('Environment variable RESOURCE_URL_COUNTRIES not specified.');
        $data = file_get_contents($filepath) or throw new Exception('Unable to fetch file from ' . $filepath);
        $countries = json_decode($data) or throw new Exception('Error: json_decode failed');

        if (!$this->options()['quiet']) $this->info("Downloading countries...\n");
        if (!$this->options()['quiet']) $this->outputStyle->progressStart(count($countries));

        foreach ($countries as $country_key => $country_raw) {
            $country = Country::firstOrCreate(
                [
                    'iso' => $country_raw->cca3,
                ],
                [
                    'name' => $country_raw->name->official,
                    'common_name' => $country_raw->name->common,
                    'population' => $country_raw->population,
                    'flag_path' => $country_raw->flags->svg,
                    'timezone' => collect($country_raw->timezones)->implode(', '),
                ]
            );

            $currencies = array();
            foreach($country_raw->currencies as $currency_key => $currency_raw) {
                $currency = Currency::firstOrCreate(
                    [
                        'iso' => $currency_key,
                    ],
                    [
                        'name' => $currency_raw->name,
                        'sign' => $currency_raw->symbol,
                    ]
                );
                $currencies[] = $currency->id;
            }

            $country->currencies()->sync($currencies);
            if (!$this->options()['quiet'])  $this->outputStyle->progressAdvance();
        }

        if (!$this->options()['quiet']) $this->outputStyle->progressFinish();
    }
}
