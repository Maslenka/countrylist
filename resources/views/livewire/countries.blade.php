<div class="grid">
    <div class="countries">
        <div class="countries_inner">
            @forelse ($countries as $country_list)
                <div class="country" wire:click="showCountry({{ $country_list->id }})">
                    {{ $country_list->common_name }}
                </div>
            @empty
                <div class="country">
                    There were no countries available.
                </div>
            @endforelse
        </div>
    </div>

    <div class="country_info">
        <div class="country_info_inner">
            @if ($country_selected)
                <img src="{{ $country->flag_path }}" alt="">

                <div class="name">{{ $country->name }}</div>
                <div class="name-common">
                    <div class="label">Common name:</div>
                    {{ $country->common_name }}
                </div>

                <div class="timezones">
                    <div class="label">Timezones:</div>
                    {{ $country->timezone }}
                </div>

                <div class="population">
                    <div class="label">Population:</div>
                    {{ number_format($country->population) }}
                </div>

                <div class="currencies">
                    <div class="name">Currencies</div>

                    @forelse ($country->currencies as $currency)
                        <div class="currency">
                            {{ $currency->iso }} (<span>{{ $currency->name }})</span>
                            @if ($currency->id == 5)
                                <p>This is reference currency.</p>
                            @elseif (array_key_exists($currency->id, $chart_data))
                                <canvas id="lineChart{{ $currency->id }}"></canvas>
                            @else
                                <p>No currency rate available.</p>
                            @endif
                        </div>
                    @empty
                        <div class="currency">
                            There are no currencies available.
                        </div>
                    @endforelse

                </div>

            @else
                Please select country.
            @endif
        </div>
    </div>

</div>
