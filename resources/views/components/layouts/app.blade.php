<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>{{ $title ?? 'Country List' }}</title>

        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

        <link rel="stylesheet" href="{{ asset('css/styles.css') }}">

        <script>
            var chartData;
            var lastUpdated = Date.now();
            var lastChanged = Date.now();
            var renderedCharts = [];

            function updateCharts() {
                if (typeof chartData !== 'undefined' && lastChanged > lastUpdated) {

                    renderedCharts.forEach(function(item, index) {
                        item.destroy();
                    });

                    Object.keys(chartData).forEach(key => {
                        const value = chartData[key];

                        var canvas = document.getElementById('lineChart' + key);
                        if (canvas) {
                            var ctx = canvas.getContext('2d');

                            lastUpdated = Date.now();

                            var myChart = new Chart(ctx, {
                                type: 'line',
                                data: {
                                    labels: value['labels'],
                                    datasets: [{
                                        label: 'Conversion rate against EUR',
                                        data: value['data'],
                                        borderColor: 'rgba(75, 192, 192, 1)',
                                        borderWidth: 1,
                                        fill: false
                                    }]
                                },
                            });
                            renderedCharts.push(myChart);
                        }
                    })
                }
            };

            // Re-initialize charts
            window.addEventListener("load", (event) => {
                // Select the node that will be observed for mutations
                const targetNode = document.getElementsByTagName("body")[0];

                // Options for the observer (which mutations to observe)
                const config = { attributes: true, childList: true, subtree: true };

                // Callback function to execute when mutations are observed
                const callback = (mutationList, observer) => {
                    for (const mutation of mutationList) {
                        if (mutation.type === "childList") {
                            console.log("A child node has been added or removed.");
                            updateCharts();
                        }
                    }
                };

                // Create an observer instance linked to the callback function
                const observer = new MutationObserver(callback);

                // Start observing the target node for configured mutations
                observer.observe(targetNode, config);
            });

            window.addEventListener('contentChanged', (event) => {
                // This runs before DOM gets modified, hence the observer above.
                // Chances are there are better ways to propagate events.
                chartData = event.detail.charts;
                updateCharts();
                lastChanged = Date.now();
            })
        </script>
    </head>
    <body>
        {{ $slot }}
    </body>
</html>
