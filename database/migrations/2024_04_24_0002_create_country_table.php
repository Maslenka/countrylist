<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->id();
            $table->string('iso');
            $table->string('name');
            $table->string('common_name');
            $table->unsignedInteger('population');
            $table->string('flag_path');
            $table->string('timezone'); /* neviem, kvoli comu toto ukladame a tutiz zvolit vhodny format (?) */

            $table->timestamps();
        });

        Schema::create('country_currencies', function (Blueprint $table) {
            $table->id();
            $table->unsignedBiginteger('country_id');
            $table->unsignedBiginteger('currency_id');

            $table->foreign('country_id')->references('id')
                ->on('countries')->onDelete('cascade');
            $table->foreign('currency_id')->references('id')
                ->on('currencies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country_currencies');
        Schema::dropIfExists('countries');

    }
}
