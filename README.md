# Country list

Use this app to fetch country list with respective currency lists from [restcountries.com](https://restcountries.com) and list them with list of currency rates obtained from ECB.

## Configuration

Make sure database and following variables are correctly set up in your .env file:
```
RESOURCE_URL_COUNTRIES="https://restcountries.com/v3.1/all?fields=currencies,population,timezones,flags,name,cca3"
RESOURCE_URL_CURRENCY_RATES="https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml"
RESOURCE_URL_CURRENCY_RATES_90="https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml"
```

## Instalation
Run composer and database migration

```
composer global require "laravel/installer"
composer install
php artisan migrate
php artisan serve
```

## Fetch country list
`php artisan download:countries`

## Fetch currency rates
Fetch latest record with
`php artisan download:currency_rates`

Fetch records for last 90 days with
`php artisan download:currency_rates --last-90-days`


### Fetch currency rates daily
Either use laravel scheduler or use cron to run the command let's say at 4 AM

`* 4 * * * cd /path-to-your-project && php artisan download:currency_rates --quiet >> /dev/null 2>&1
`
